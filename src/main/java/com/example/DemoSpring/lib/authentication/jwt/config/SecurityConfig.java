package com.example.DemoSpring.lib.authentication.jwt.config;

import com.example.DemoSpring.lib.authentication.jwt.rest.CustomAccessDeniedHandler;
import com.example.DemoSpring.lib.authentication.jwt.rest.JwtAuthenticationTokenFilter;
import com.example.DemoSpring.lib.authentication.jwt.rest.RestAuthenticationEntryPoint;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

@Configuration
@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {
    @Bean
    public JwtAuthenticationTokenFilter jwtAuthenticationTokenFilter() throws Exception {
        JwtAuthenticationTokenFilter jwtAuthenticationTokenFilter = new JwtAuthenticationTokenFilter();
        jwtAuthenticationTokenFilter.setAuthenticationManager(authenticationManager());
        return jwtAuthenticationTokenFilter;
    }

    @Bean
    public RestAuthenticationEntryPoint restServiceEntryPoint() {
        return new RestAuthenticationEntryPoint();
    }

    @Bean
    public CustomAccessDeniedHandler customAccessDeniedHandler() {
        return new CustomAccessDeniedHandler();
    }

    @Bean
    @Override
    protected AuthenticationManager authenticationManager() throws Exception {
        return super.authenticationManager();
    }

    protected void configure(HttpSecurity http) throws Exception {
//        http.csrf().ignoringAntMatchers("/user/**");

        http
                .authorizeRequests()
                .antMatchers("/user/login")
                .permitAll();

//        http
//                .authorizeRequests()
//                .anyRequest()
//                .authenticated();

//        http.authorizeRequests().antMatchers("/user").permitAll();

//        http.antMatcher("/user/**")
//                .httpBasic().authenticationEntryPoint(restServiceEntryPoint())
//                .and()
//                .sessionManagement()
//                .sessionCreationPolicy(SessionCreationPolicy.STATELESS)
////                .and()
////                .authorizeRequests()
////                .antMatchers(HttpMethod.DELETE, "/user/**").access("hasRole('ROLE_ADMIN') or hasRole('ROLE_USER')")
//                .and()
//                .addFilterBefore(jwtAuthenticationTokenFilter(), UsernamePasswordAuthenticationFilter.class)
//                .exceptionHandling().accessDeniedHandler(customAccessDeniedHandler());
    }
}
