package com.example.DemoSpring.lib.authentication.jwt.service;

import com.nimbusds.jose.JWSAlgorithm;
import com.nimbusds.jose.JWSHeader;
import com.nimbusds.jose.JWSSigner;
import com.nimbusds.jose.JWSVerifier;
import com.nimbusds.jose.crypto.MACSigner;
import com.nimbusds.jose.crypto.MACVerifier;
import com.nimbusds.jwt.JWTClaimsSet;
import com.nimbusds.jwt.SignedJWT;
import org.springframework.stereotype.Service;

import java.util.Date;

@Service
public class JwtService {
    public static final String USER_NAME = "username";
    // 32-bit
	public static final String SECRET_KEY = "01234567891011121314151617181920";
    public static final int EXPIRED_DATE = 86400000;

    public String generateToken(String email) {
        String token = null;

        try {
            // Create HMAC signer
            JWSSigner signer = new MACSigner(generateShareSecret());

            // Create claim set info
            JWTClaimsSet.Builder builder = new JWTClaimsSet.Builder();
            JWTClaimsSet claimsSet = builder
                    .subject("test")
                    .expirationTime(new Date(EXPIRED_DATE))
                    .claim(USER_NAME, email)
                    .build();

            // Sign jwt
            SignedJWT signedJWT = new SignedJWT(new JWSHeader(JWSAlgorithm.HS256), claimsSet);

            // Signed jwt
            signedJWT.sign(signer);
            token = signedJWT.serialize();

        } catch (Exception e) {
            e.printStackTrace();
        }

        return token;
    }

    public Boolean validateTokenLogin(String token) {
        if(token == null || token.trim().isEmpty()) {
            return false;
        }

        String userEmail = getEmailUserFromToken(token);
        if(userEmail == null || userEmail.isEmpty()) {
            return false;
        }

        if(isTokenExpired(token)) {
            return false;
        }

        return true;
    }

    public String getEmailUserFromToken(String token) {
        String emailUser = null;
        try {
            JWTClaimsSet jwtClaimsSet = getClaimFromToken(token);
            emailUser = jwtClaimsSet.getStringClaim(USER_NAME);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return emailUser;
    }

    private Boolean isTokenExpired(String token) {
        Date expiration = getExpirationDate(token);
        return expiration.before(new Date());
    }

    private Date getExpirationDate(String token) {
        JWTClaimsSet jwtClaimsSet = getClaimFromToken(token);
        return jwtClaimsSet.getExpirationTime();
    }

    private JWTClaimsSet getClaimFromToken(String token) {
        JWTClaimsSet jwtClaimsSet = null;

        try {
            SignedJWT signedJWT = SignedJWT.parse(token);
            JWSVerifier verifier = new MACVerifier(generateShareSecret());

            if(signedJWT.verify(verifier)) {
                jwtClaimsSet = signedJWT.getJWTClaimsSet();
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return jwtClaimsSet;
    }

    private byte[] generateShareSecret() {
        // Generate 256-bit (32-byte) shared secret
        byte[] sharedSecret = new byte[32];
        sharedSecret = SECRET_KEY.getBytes();

        return sharedSecret;
    }

}
