package com.example.DemoSpring.common;

import lombok.Getter;

@Getter
public enum ErrorResponseConst {
    ACCESS_DENIED("001", "Access deniedddddddd!"),
    UNAUTHORIZED("002", "Unauthorized");

    private String code;
    private String message;
    private ErrorResponseConst(String code, String message) {
        this.code = code;
        this.message = message;
    }
}
