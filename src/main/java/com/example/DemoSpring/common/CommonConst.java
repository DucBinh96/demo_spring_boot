package com.example.DemoSpring.common;

import lombok.Getter;

@Getter
public enum CommonConst {
    TOKEN_HEADER("authorization");

    private String name;
    private CommonConst(String name) {
        this.name = name;
    }
}
