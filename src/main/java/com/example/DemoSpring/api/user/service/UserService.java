package com.example.DemoSpring.api.user.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.DemoSpring.api.user.model.User;
import com.example.DemoSpring.api.user.repository.UserRepository;

@Service
public class UserService{
	@Autowired
	UserRepository userRepo;
	
	public List<User> getAllUser() {
		return userRepo.findAll();
	}
	
	public User createUser(User user) {
		return userRepo.save(user);
	}

	public User getUserLogin(String email, String password) {
		return userRepo.getUserLogin(email, password);
	}

	public User getUserByEmail(String email) {
		return userRepo.getUserByEmail(email);
	}
}
