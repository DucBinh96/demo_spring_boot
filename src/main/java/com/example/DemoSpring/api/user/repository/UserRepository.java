package com.example.DemoSpring.api.user.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.example.DemoSpring.api.user.model.User;
import com.example.DemoSpring.lib.db.IRepository;

@Repository
public interface UserRepository extends IRepository<User, Long> {
    @Query(value = "SELECT * FROM users WHERE email=:email AND password=:password", nativeQuery = true)
	User getUserLogin(@Param("email") String email, @Param("password") String password);

    @Query(value = "SELECT * FROM users WHERE email=:email", nativeQuery = true)
    User getUserByEmail(@Param("email") String email);
}
