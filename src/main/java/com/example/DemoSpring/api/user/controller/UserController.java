package com.example.DemoSpring.api.user.controller;

import java.util.List;
import java.util.UUID;

import com.example.DemoSpring.lib.authentication.jwt.service.JwtService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.example.DemoSpring.api.user.model.User;
import com.example.DemoSpring.api.user.service.UserService;

@RestController
@RequestMapping(value = "/user")
public class UserController {
	@Autowired
	UserService userService;

	@Autowired
	JwtService jwtService;

	@RequestMapping(value="/users", method=RequestMethod.GET)
	public ResponseEntity<List<User>> getAllUser() {
		return new ResponseEntity<List<User>>(userService.getAllUser(), HttpStatus.OK);
	}

	// For test so use method get
	@RequestMapping(value="/create", method=RequestMethod.GET)
	public ResponseEntity<User> create() {
		User user = new User();
		user.setId(UUID.randomUUID());
		user.setEmail("kieubinh96@gmail.com");
		user.setName("binhkieu");
		user.setRole("ROLE_USER");
		user.setPassword("123456");
		
		return new ResponseEntity<User>(userService.createUser(user), HttpStatus.OK);
	}

	@RequestMapping(value="/login", method=RequestMethod.GET)
	public ResponseEntity<String> login() {
		String email = "kieubinh96@gmail.com";
		String password = "123456";

		User user = userService.getUserLogin(email, password);
		return new ResponseEntity<String>(jwtService.generateToken(email), HttpStatus.OK);
	}
}
