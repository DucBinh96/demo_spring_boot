package com.example.DemoSpring.api.user.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Getter;
import lombok.Setter;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(value = {"roles", "authorities", "password"})
@Entity
@Table(name = "users")
@Getter
@Setter
public class User implements Serializable {
	private static final long serialVersionUID = -297553281792804396L;
	
	@Id
	private UUID id;
	
	@Column(name="email")
	private String email;
	
	@Column(name="name")
	private String name;
	
	@Column(name="age")
	private int age;
	
	@Column(name="address")
	private String address;
	
	@Column(name="password")
	private String password;

	@Column(name = "role")
	private String role;
	
	public User() {
		// TODO Auto-generated constructor stub
	}

	public List<GrantedAuthority> getAuthorities() {
		List<GrantedAuthority> authorities = new ArrayList<>();

//		for(String role : roles) {
		authorities.add(new SimpleGrantedAuthority(role));
//		}

		return authorities;
	}
}
